# Contributor: Galen Abell <galen@galenabell.com>
# Maintainer: Galen Abell <galen@galenabell.com>
pkgname=actionlint
pkgver=1.7.6
pkgrel=0
pkgdesc="Static checker for GitHub Actions workflow files"
url="https://github.com/rhysd/actionlint"
arch="all"
license="MIT"
makedepends="go ronn"
options="chmod-clean net"
subpackages="$pkgname-doc"
source="$pkgname-$pkgver.tar.gz::https://github.com/rhysd/actionlint/archive/refs/tags/v$pkgver.tar.gz"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build \
		-ldflags="-buildid= -X github.com/rhysd/actionlint.version=$pkgver -s -w" \
		./cmd/$pkgname
	ronn ./man/$pkgname.1.ronn
}

check() {
	# Some tests expect to be run from a git repository.
	git init --quiet
	go test ./...
	rm -rf .git
}

package() {
	install -Dm755 $pkgname "$pkgdir"/usr/bin/$pkgname
	install -Dm644 man/$pkgname.1 "$pkgdir"/usr/share/man/man1/$pkgname.1
	install -Dm644 LICENSE.txt "$pkgdir"/usr/share/licenses/$pkgname/LICENSE
}

sha512sums="
0518491afd2452588de70cbbfde70513d19908691e39792d2b0ab184d60641bf870b59550665dfdd8c07e183e45b1c34ba471143bc484e10aaaac2c8af04c024  actionlint-1.7.6.tar.gz
"
